var express = require('express');
var app = express();
var http = require('http').Server(app);
var elasticsearch = require('elasticsearch');
var mongojs = require("mongojs");
var bodyParser=require("body-parser");
var router = express.Router();
var db = mongojs("belajarmean",["bukutamu"]);
var client = elasticsearch.Client({
  host: 'localhost:9200'
})
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
//app.set('view engine', 'jade');
app.use(express.static(__dirname + '/views'));
app.use('/public',express.static(__dirname + '/public'));
app.use('/bower_components',express.static(__dirname + '/bower_components'));

client.search({
  index: 'books',
  type: 'book',
  body: {
    query: {
      multi_match: {
        query: 'express js',
        fields: ['title', 'description']
      }
    }
  }
}).then(function (response) {
  var hits = response.hits.hits
}, function (error) {
  console.trace(error.message)
})
app.get("/data/api",function(req,res){
  db.bukutamu.find(function(err,docs){
    res.json(docs);
  })
})
app.post("/root/data/api",function(req,res){
  db.bukutamu.insert(req.body,function(){
    res.end()
  })
})
app.delete("/root/data/api/:id",function(req,res){
  var id = req.params.id;
  db.bukutamu.remove({_id:mongojs.ObjectId(id)},function(err,docs){
    res.end()
  })
})
  app.put("/root/data/api",function(req,res){
    db.bukutamu.update({_id:mongojs.ObjectId(req.body.id)},{$set:{
    nama:req.body.nama,
    alamat:req.body.alamat,
    nomor:req.body.nomor
  }},function(err,docs){
    res.end();
  });
  })
http.listen(3000)